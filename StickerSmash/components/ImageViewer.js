import { StyleSheet, Image } from "react-native";

export default function ImageViewer({PlaceHolderImageSource, selectedImage}){
    const imageSource = selectedImage !== null
    ? { uri: selectedImage }
    : PlaceHolderImageSource;

    return(
        <Image source={imageSource} style={styles.image}></Image>
    );
}


const styles = StyleSheet.create({
    image: {
        width: 320,
        height: 440,
        borderRadius: 18,
      }
})